# Outils formels de modélisation @ University of Geneva

This page contains important information about the course.

## Work environment for the exercise sessions

A significant part of the exercise sessions will be conducted on a computer.
Therefore, each student will be required to have access to the following work environment:

* A computer installed with Ubuntu 18.04 or macOS 10.13 (High Sierra).
* A `git` client, accessible from the command line.
* Access to your [university GitLab](https://gitlab.unige.ch/smv-ofa-2018/information) account.

You are free to choose another operating system, but note that
**you won't receive support from the course's assistant for any system-related issue**
if you chose to do so.

### Setup your SSH keys on GitLab

So as to be able to clone, pull and push updates to GitLab repositories,
you'll need GitLab to know your identity.
The best way to do so is to [register your SSH keys](https://docs.gitlab.com/ee/ssh/).

1. Generate a new SSH key
2. Add your SSH key to the ssh-agent
3. Add your SSH key to your GitLab account

### Get and submit homeworks

In order to get the necessary files for your homeworks, and also to submit them,
you will have to fork this repository.
Proceed as follows:

1. Click on the **Fork** button at the top of the homework repository homepage on GitLab.
   This will create a fork (or copy) if the repository on your own account.
2. Clone **your own** repository (that is the one you just forked) on your machine:
   `git clone git@gitlab.com:<your-username>/smv-ofa-2018/<homework>.git`.

This will create a local copy of **your own** repository on your machine.
You can freely work on this local copy,
and update any file you like,
including, of course, those required to complete your homeworks.

Submissions must be made by merge request.
Proceed as follows:

1. TBD ...
